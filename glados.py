#coding:utf-8

import telegram
import os
import random
import logging

logging.basicConfig(level=logging.DEBUG, format='%(levelname) -8s %(message)s')

def caption(bot, update):
	path = os.getcwd()+"/ogg"
	oggs = os.listdir(path)
	nb = random.randint(0,len(oggs))
	bot.sendAudio(chat_id=update.message.chat_id, audio=open("ogg/"+oggs[nb], 'rb'))	

def main():
	token=input("Entrez le token : ")
	updater = telegram.Updater(token)

	dp = updater.dispatcher

	dp.addTelegramCommandHandler("caption", caption)

	updater.start_polling()

	updater.idle()

if __name__ == '__main__':
    main()
